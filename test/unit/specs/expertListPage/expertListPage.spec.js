import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import ExpertListPage from '@/pages/ExpertListPage'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)
const router = new VueRouter()

describe('ExpertListPage', () => {
  const getExpertCounties = jest.fn()
  const getExperts = jest.fn()

  const store = new Vuex.Store({
    modules: {
      expertList: {
        state: {
          isLoading: false,
          counties: [],
          selected: { county: 'All Experts', id: 0 }
        },

        actions: { getExpertCounties, getExperts },

        getters: { currentPage: () => ({ data: 2 }) }
      }
    }
  })

  const wrapper = shallowMount(ExpertListPage, {
    mocks: {
      $route: { params: { county: 2 } }
    },
    localVue,
    store,
    router
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should call "getExpertCounties" on mounting', () => {
    expect(getExpertCounties).toHaveBeenCalledTimes(1)
  })

  it('should call "getExperts"', () => {
    wrapper.vm.handleCountySelection('experts')
    expect(getExperts).toHaveBeenCalledTimes(1)
  })

  it('should call "getExperts" when pagination params are provided', () => {
    wrapper.vm.handlePagination('params')
    expect(getExperts).toHaveBeenCalled()
  })
})
