import AxiosCalls from '@/utils/api/AxiosCalls';
// eslint-disable-next-line camelcase
import jwt_decode from 'jwt-decode';

export default {
  state: {
    user: {},
    error: null,
    isLoading: false,
    profilePic: require('@/assets/profile.png')
  },
  getters: {
    USER_PROFILE (state) {
      return { ...state.user }
    },
    USER_PROFILE_ERROR (state) {
      return state.error
    },
    USER_PROFILE_LOADING (state) {
      return state.isLoading
    },
    PROFILE_PIC (state) {
      return state.profilePic
    }
  },
  mutations: {
    SET_USER_PROFILE (state, user) {
      state.user = user
    },
    SET_USER_PROFILE_ERROR (state, error) {
      state.error = error
    },
    SET_IS_LOADING (state, payload) {
      state.isLoading = payload
    },
    SET_PROFILE_PIC (state, payload) {
      state.profilePic = payload
    }
  },
  actions: {
    GET_USER_PROFILE: async context => {
      const token = localStorage.getItem('token')
      const userData = jwt_decode(token)
      const userId = userData.sub
      try {
        context.commit('SET_IS_LOADING', true)
        let { data } = await AxiosCalls.get(`api/v1/user/${userId}`)
        context.commit('SET_USER_PROFILE', data.data)
        context.commit('SET_PROFILE_PIC', data.data.profile.pic)
        context.commit('SET_IS_LOADING', false)
        return data.data
      } catch (error) {
        throw error
      }
    }
  }
}
