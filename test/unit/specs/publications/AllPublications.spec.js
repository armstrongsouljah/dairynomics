import { createLocalVue, mount } from '@vue/test-utils'
import Vuex from 'vuex'
import Router from 'vue-router'
import mockPublications from './__mock__/publications'
import mockUser from './__mock__/user'
import testHelpers from '@/utils/testHelpers'
import AllPublications from '@/pages/Publications/AllPublications'
import PublicationListItem from '@/pages/Publications/PublicationListItem'
import PublicationNavHeader from '@/pages/Publications/PublicationNavHeader'
import PublicationsList from '@/pages/Publications/PublicationsList'

const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(Router)

describe('AllPublications Component', () => {
  let wrapper
  let store
  let actions
  let currentPage = 1
  let lastPage = 5
  let state = {
    user: mockUser,
    allPublications: {
      data: mockPublications,
      pagination: {
        currentPage,
        lastPage,
        total: 5
      },
      paginatedData: {}
    },
    featuredPublications: {
      data: [],
      pagination: {
        currentPage,
        lastPage,
        total: 5
      },
      paginatedData: {}
    },
    requestStatuses: {
      isDownloading: false,
      isFetching: false
    }
  }

  const getters = {
    getPaginatedAllPublications: publicationsState => publicationsState.allPublications.paginatedData,
    getAllPublications: publicationsState => publicationsState.allPublications.data,
    getAllPublicationsPagination: publicationsState => publicationsState.allPublications.pagination,
    getRequestStatuses: publicationsState => publicationsState.requestStatuses,
    USER_PROFILE: profileState => profileState.user
  }

  const mountComponent = () => {
    actions = {
      fetchPublications: jest.fn(),
      downloadPublication: jest.fn(),
      incrementPage: jest.fn(),
      decrementPage: jest.fn(),
      GET_USER_PROFILE: jest.fn()
    }

    state.allPublications.paginatedData[`page-${currentPage}`] = mockPublications
    store = new Vuex.Store({ state, getters, actions })
    const router = new Router({
      routes: [{
        path: '/publications',
        name: 'AllPublications',
        component: AllPublications,
        meta: {
          isAuth: true
        }
      }]
    })
    wrapper = mount(AllPublications, { store, localVue, router })
    testHelpers.wrapper = wrapper
    return wrapper
  }

  it('does not show publication items when publication list is empty', () => {
    currentPage = undefined
    mountComponent()
    testHelpers.notSeeElement('.pub-list-item')
    testHelpers.notSeeElement(PublicationListItem)
  })

  it('lists publications items when item exist in list ', () => {
    currentPage = 1
    wrapper = mountComponent()
    testHelpers.seeElement(PublicationListItem)
    testHelpers.seeString(mockPublications[0].title)
    testHelpers.seeElement('.pub-list-item')
  })

  it('shows loader when data is fetching', () => {
    currentPage = 1
    wrapper = mountComponent()
    testHelpers.seeElement(PublicationsList)

    const pubList = wrapper.find(PublicationsList)
    pubList.vm.isFetching = true
    expect(pubList.vm.loaderContent).toBe('<i class="fas fa-spinner fa-spin"/>')
    pubList.vm.isFetching = false
    expect(pubList.vm.loaderContent).toBe('')
  })

  it('handles actions called on clicks', async () => {
    currentPage = 2
    wrapper = mountComponent()
    expect(actions.fetchPublications).toHaveBeenCalled()

    // download button actions
    let button = wrapper.find('.pub-list-item .action button')
    await button.trigger('click')
    expect(actions.downloadPublication).toHaveBeenCalled()

    // pagination buttons
    // previous button click
    button = wrapper.find('.pagination-actions .align-left button')
    await button.trigger('click')
    expect(actions.decrementPage).toHaveBeenCalled()

    // next button click
    button = await wrapper.find('.pagination-actions .align-right button')
    await button.trigger('click')
    expect(actions.incrementPage).toHaveBeenCalled()

    // view single publication div click
    // mocking data for PublicationListItem
    const publication = wrapper.find(PublicationListItem)
    publication.vm.$router.push = jest.fn()
    publication.vm.downloadClicked = true
    // swtich the values of isDownloading to trigger code
    // within the watch block
    publication.vm.isDownloading = true
    publication.vm.isDownloading = false

    button = await wrapper.find('.bottom-section .info .name')
    await button.trigger('click')
    expect(publication.vm.$router.push).toHaveBeenCalled()

    // PublicationNavHeader
    // mocking data for PublicationNavHeader
    const publicationNav = wrapper.find(PublicationNavHeader)
    publicationNav.vm.$router.push = jest.fn()

    const button1 = await wrapper.find('.publication-header > span:first-child')
    const button2 = await wrapper.find('.publication-header > span:last-child')
    await button1.trigger('click')
    await button2.trigger('click')
    expect(publicationNav.vm.$router.push).toHaveBeenCalledTimes(2)
  })

  it('handles pagination scenario where currentPage === lastPage', () => {
    currentPage = lastPage
    state.allPublications.pagination.currentPage = currentPage
    wrapper = mountComponent()

    testHelpers.seeElement(PublicationListItem)
    testHelpers.seeElement('.pub-list-item')
  })

  it('handles pagination scenario where currentPage === 1', () => {
    currentPage = lastPage
    state.allPublications.pagination.currentPage = currentPage
    wrapper = mountComponent()

    testHelpers.seeElement(PublicationListItem)
    testHelpers.seeElement('.pub-list-item')
  })
})
