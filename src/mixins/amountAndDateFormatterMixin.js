import moment from 'moment'

export const amountAndDateFormatterMixin = {
  methods: {
    getAmount (amount = 0, currency = 'KES') {
      return `${currency} ${parseInt(amount, 10).toLocaleString()}`
    },
    getDate (date) {
      return moment(date).format('MMM Do, Y')
    }
  }
}
