import { shallowMount, createLocalVue } from '@vue/test-utils'
import App from '@/App.vue'
import AppNavigationBar from '@/components/layout/AppNavigationBar.vue'

const localVue = createLocalVue()

describe('@/App.vue', () => {
  const wrapper = shallowMount(App, {
    stubs: ['router-link', 'router-view', 'notifications'],
    localVue
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('contains a navigation bar component', () => {
    expect(wrapper.contains(AppNavigationBar)).toBe(true)
  })
})
