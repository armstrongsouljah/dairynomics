const updates = [{
  'id': '151',
  'user': {
    'id': '1',
    'first_name': 'Mark',
    'last_name': 'Kyalo',
    'email': 'kyalo@cowsoko.com',
    'country': '',
    'country_code': '254',
    'phone': '0703273842',
    'account_type': '',
    'county': 'Elgeyo-Marakwet',
    'profile': {
      'pic': 'https://beta.cowsoko.com/storage/img/avatar_faceorder_1528966010_5eefe.png',
      'bio': 'bio bio bio',
      'social': {
        'facebook': 'fgffg',
        'twitter': 'fgfgfg'
      }
    },
    'is_seller': 0,
    'is_expert': '1',
    'expert_category': 'Biogas',
    'course_studied': 'degree in western spaghetti',
    'institution': 'University of Nairobi',
    'skills': 'Dairy Business plans,Feeding and homemade rations,Silage',
    'training_level': 'Certificate',
    'start_date': '2000-03-02',
    'finish_date': '2012-12-12',
    'approved_expert': 1,
    'approved_seller': 0,
    'verified_account': 0,
    'number_of_blogs': 0,
    'number_of_updates': 10,
    'number_of_farms': 0,
    'number_of_livestocks': 0,
    'view_counts': 878
  },
  'photo': [
    'https://beta.cowsoko.com/storage/img/'
  ],
  'text': 'test',
  'likes': 0,
  'user_likes': [],
  'number_of_comments': 0,
  'topic': null,
  'source': 'expert',
  'created_at': '2018-09-24 17:17:22',
  'updated_at': {
    'date': '2018-09-24 17:17:22.000000',
    'timezone_type': 3,
    'timezone': 'Africa/Nairobi'
  },
  'facebook_share': 'https://www.facebook.com/sharer/sharer.php?u=https://beta.cowsoko.com/api/v1/updates/151',
  'twitter_share': 'https://twitter.com/share?url=https://beta.cowsoko.com/api/v1/updates/151&text=test'
}]

export default updates
