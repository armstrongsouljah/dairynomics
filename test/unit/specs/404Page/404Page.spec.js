import { shallowMount, createLocalVue } from '@vue/test-utils'
import OfflineIndicator from '@/pages/404Page/404Page.vue'

const localVue = createLocalVue()

describe('404Page', () => {
  let wrapper

  it('should render the offlineIndicator component', () => {
    wrapper = shallowMount(OfflineIndicator, { localVue })
    expect(wrapper.isVueInstance).toBeTruthy()
  })
})
