import AxiosCalls from '@/utils/api/AxiosCalls'
import uniqueBy from 'unique-by'

export default {
  state: {
    mainUpdates: {
      allUpdates: [],
      expertsUpdates: {
        'expert-id': []
      },
      requestStatuses: {
        isFetching: false
      },
      currentPage: 1
    }
  },
  mutations: {
    STORE_FETCHED_UPDATES ({ mainUpdates }, { data }) {
      mainUpdates.allUpdates = uniqueBy([...mainUpdates.allUpdates, ...data], 'id')
    },
    STORE_FETCHED_EXPERT_UPDATES ({ mainUpdates }, { data, expertId }) {
      const expertData = mainUpdates.expertsUpdates[expertId] || []
      const expertsUpdatesData = uniqueBy([...expertData, ...data], 'id')
      mainUpdates.expertsUpdates[expertId] = expertsUpdatesData
    },
    CHANGE_REQUEST_STATUS ({ mainUpdates }, payload) {
      mainUpdates.requestStatuses = {
        ...mainUpdates.requestStatuses,
        ...payload
      }
    },
    CHANGE_CURRENT_PAGE_NUMBER ({ mainUpdates }, payload) {
      mainUpdates.currentPage = payload.currentPage
    }
  },
  actions: {
    async fetchUpdates ({ commit }, { pageNumber = 1, dataScope = 'allUpdates', expertId = null }) {
      commit('CHANGE_REQUEST_STATUS', { isFetching: true })

      let requestsMap = {
        allUpdates: 'api/v1/updates',
        expertUpdates: `api/v1/experts/${expertId}/updates`
      }

      try {
        const token = localStorage.getItem('token')
        const endpoint = requestsMap[dataScope]
        const url = `${endpoint}?page=${pageNumber}`
        let { data: responseData } = await AxiosCalls.get(url, token)

        commit('CHANGE_REQUEST_STATUS', { isFetching: false })
        commit('CHANGE_CURRENT_PAGE_NUMBER', { currentPage: responseData.meta.current_page })

        let commitsMap = {
          allUpdates: () => commit('STORE_FETCHED_UPDATES', { data: responseData.data.updates }),
          expertUpdates: () => commit('STORE_FETCHED_EXPERT_UPDATES', { data: responseData.data.updates, expertId })
        }

        const matchedCommit = commitsMap[dataScope]
        matchedCommit()
      } catch (error) {
        commit('CHANGE_REQUEST_STATUS', { isFetching: false })
      }
    }
  },
  getters: {
    getAllUpdates: state => state.mainUpdates.allUpdates,
    getExpertsUpdates: state => state.mainUpdates.expertsUpdates,
    getUpdatesRequestStatuses: state => state.mainUpdates.requestStatuses,
    getCurrentPage: state => state.mainUpdates.currentPage
  }
}
