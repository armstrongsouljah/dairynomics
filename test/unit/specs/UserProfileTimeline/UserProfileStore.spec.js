import axios from 'axios'
import UpdateProfileStore from '@/store/modules/UpdateProfileStore'
import AxiosMockAdapter from 'axios-mock-adapter'

jest.mock('@/router/router')
describe('setUserUpdateStatus mutation', () => {
  it('authenticated a user to edit his profile', () => {
    const state = {}
    let firstName = 'sam'
    let lastName = 'rubarema'
    let county = 'Igara'
    let oldPassword = '2345'
    let password = '8888'
    let confirmPassword = '8888'
    let accountType = 'I am a dairy farmer'
    let imageData = 'https://beta.cowsoko.com/storage/img/avatar_dwe_1554728809_96130.jpeg'
    UpdateProfileStore.mutations.setUserUpdateStatus(state, {firstName, lastName, county, oldPassword, password, confirmPassword, accountType, imageData})
    expect(state).toEqual({'userUpdateStatus': true})
    UpdateProfileStore.mutations.ProfileSuccess(state, {firstName, lastName, county, oldPassword, password, confirmPassword, accountType, imageData})
    expect(state).toEqual({successMessage: {accountType: 'I am a dairy farmer', confirmPassword: '8888', county: 'Igara', firstName: 'sam', imageData: 'https://beta.cowsoko.com/storage/img/avatar_dwe_1554728809_96130.jpeg', lastName: 'rubarema', oldPassword: '2345', password: '8888'}, updateSuccess: true, userUpdateStatus: true})
  })
})
describe('UpdateStop mutation', () => {
  it('When a user fails to update his or her profile', () => {
    const state = {}
    let firstName = 'sam'
    let lastName = 'rubarema'
    let county = 'Igara'
    let oldPassword = '2345'
    let password = '8888'
    let confirmPassword = '8888'
    let accountType = 'I am a dairy farmer'
    let imageData = 'https://beta.cowsoko.com/storage/img/avatar_dwe_1554728809_96130.jpeg'
    UpdateProfileStore.mutations.UpdateErrors(state, {firstName, lastName, county, oldPassword, password, confirmPassword, accountType, imageData})
    expect(state).toEqual({'updateError': {'accountType': 'I am a dairy farmer', 'confirmPassword': '8888', 'county': 'Igara', 'firstName': 'sam', 'imageData': 'https://beta.cowsoko.com/storage/img/avatar_dwe_1554728809_96130.jpeg', 'lastName': 'rubarema', 'oldPassword': '2345', 'password': '8888'}, 'userUpdateStatus': 0})
  })
})
describe('details mutation', () => {
  it('Fetching user profile details', () => {
    const state = {}
    let firstName = ''
    let lastName = ''
    let county = ''
    let oldPassword = ''
    let password = ''
    let confirmPassword = ''
    let accountType = ''
    let imageData = ''
    UpdateProfileStore.mutations.Details(state, {firstName, lastName, county, oldPassword, password, confirmPassword, accountType, imageData})
    expect(state).toEqual({'getUserDetails': {'accountType': '', 'confirmPassword': '', 'county': '', 'firstName': '', 'imageData': '', 'lastName': '', 'oldPassword': '', 'password': ''}})
  })
})
describe('update action', () => {
  let userId = localStorage.getItem('id')
  it('A user updates his profile', (done) => {
    let mock = new AxiosMockAdapter(axios)
    const commit = jest.fn()
    const resp = {
      firstName: 'sam',
      lastName: 'rubarema',
      county: 'Igara',
      oldPassword: '2345',
      password: '8888',
      confirmPassword: '8888',
      accountType: 'I am a dairy farmer',
      imageData: 'https://beta.cowsoko.com/storage/img/avatar_dwe_1554728809_96130.jpeg'
    }
    mock.onPost(`https://beta.cowsoko.com/api/v1/user/${userId}`).replyOnce(200, resp)
    UpdateProfileStore.actions.editUser({commit}, 'updateData')
      .then(() => {
        expect(mock.history.post[0].data).toBe('updateData')
      })
      .then(done)
      .catch(done.fail)
  })

  it('test for the  user update state getter', () => {
    const state = {userUpdateStatus: false,
      updateError: null,
      updateSuccess: false,
      getUserDetails: {}}
    UpdateProfileStore.getters.getUserUpdateStatus(state)
    expect(state).toEqual(state)
  })
  it(' A user  update profile fails', (done) => {
    let mock = new AxiosMockAdapter(axios)
    const commit = jest.fn()
    const resp = {
      firstName: 'sam',
      lastName: 'rubarema',
      county: 'Igara',
      oldPassword: '2345',
      password: '8888',
      confirmPassword: '8888',
      accountType: 'I am a dairy farmer',
      imageData: 'https://beta.cowsoko.com/storage/img/avatar_dwe_1554728809_96130.jpeg'
    }
    mock.onPost(`https://beta.cowsoko.com/api/v1/user/${userId}`).replyOnce(500, 422, resp)
    UpdateProfileStore.actions.editUser({commit}, resp)
      .then(() => {
        expect(mock.history.post.length).toBe(1)
        expect(mock.history.post[0].data).toBe('{"firstName":"sam","lastName":"rubarema","county":"Igara","oldPassword":"2345","password":"8888","confirmPassword":"8888","accountType":"I am a dairy farmer","imageData":"https://beta.cowsoko.com/storage/img/avatar_dwe_1554728809_96130.jpeg"}')
      })
      .then(done)
      .catch(done.fail)
  })
  it('should trigger the success property', () => {
    const commit = jest.fn()
    const state = {

      userUpdateStatus: false,
      updateError: null,
      updateSuccess: false,
      getUserDetails: {}
    }

    UpdateProfileStore.actions.editUser({commit})
    expect(state.updateError).toEqual(null)
  })
  it('commits userDetails mutation when the page loads', (done) => {
    let mock = new AxiosMockAdapter(axios)
    const commit = jest.fn()
    const userId = localStorage.getItem('id')

    mock.onGet(`https://beta.cowsoko.com/api/v1/user/${userId}`).reply(200, {
      user: {
        id: '1878',
        first_name: 'Njunge',
        last_name: 'Njenge',
        email: null,
        country: 'Kenya',
        country_code: '254',
        phone: '0726325093',
        account_type: 'I am a dairy farmer',
        county: null,
        profile: {
          pic: 'https://beta.cowsoko.com/storage/img/avatar_rub_1554813271_823f7.jpeg',
          bio: null,
          social: {
            facebook: null,
            twitter: null
          }
        },
        verified_account: 1
      }
    })

    UpdateProfileStore.actions.userDetails({commit}).then(() => {
      expect(commit).toHaveBeenCalledTimes(1)
      done()
    })
  })
  it('commit updateErrors mutation when there is an error', (done) => {
    const commit = jest.fn()
    const userId = localStorage.getItem('id')
    let mock = new AxiosMockAdapter(axios)

    mock.onPost(`https://beta.cowsoko.com/api/v1/user/${userId}`).replyOnce(500)

    UpdateProfileStore.actions.editUser({commit}).catch(() => {
      expect(commit).toHaveBeenCalledTimes(2)
      done()
    })
  })
})
