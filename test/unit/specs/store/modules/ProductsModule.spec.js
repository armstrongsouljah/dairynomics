import productsModule from '@/store/modules/productsModule'

jest.mock('@/store')

describe('products store module', () => {
  describe('products module mutation', () => {
    describe('STORE_COUNTIES mutation', () => {
      const state = { counties: [] }
      it('it should set the counties', () => {
        productsModule.mutations.STORE_COUNTIES(state, 'bomet')
        expect(state).toEqual({ counties: 'bomet' })
      })
    })

    describe('STORE_PRODUCTS mutation', () => {
      const state = { products: [] }
      it('it should set the products', () => {
        productsModule.mutations.STORE_PRODUCTS(state, { products: ['product1', 'product2'] })
        expect(state).toEqual({
          isLoading: false,
          products: {
            products: ['product1', 'product2']
          }
        })
      })
    })

    describe('STORE_PAGINATION_INFO mutation', () => {
      const state = { paginationInfo: { currentPage: '', lastPage: '' } }
      it('it should set the pagination info', () => {
        productsModule.mutations.STORE_PAGINATION_INFO(state, { current_page: 2, last_page: 5 })
        expect(state).toEqual({ paginationInfo: { currentPage: 2, lastPage: 5 } })
      })
    })

    describe('LOADING_PRODUCTS mutation', () => {
      const state = { isLoading: false }
      it('it should set the loader to true', () => {
        productsModule.mutations.LOADING_PRODUCTS(state, true)
        expect(state).toEqual({ isLoading: true })
      })
    })
  })

  describe('Products Module actions', () => {
    let commit
    beforeEach(() => {
      commit = jest.fn()
    })

    it('should make an api call to get all counties', () => {
      productsModule.actions.fetchCounties({ commit })
      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit).toHaveBeenCalledWith('LOADING_PRODUCTS', true)
    })

    it('should make a call to cache paginated blogs', () => {
      productsModule.actions.fetchProducts({ commit }, { pageNumber: 1, county: 'kirinyaga', selectedType: 'all' })
      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit).toHaveBeenCalledWith('LOADING_PRODUCTS', true)
    })
  })
})
