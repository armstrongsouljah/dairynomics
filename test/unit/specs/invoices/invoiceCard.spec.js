import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import * as helperFunctions from '@/utils/helperFunctions'
import InvoiceCard from '@/pages/Invoices/InvoiceCard'

const localVue = createLocalVue()
localVue.use(Vuex)
const myComponent = () => {
  const getters = {
    USER_PROFILE: userprofileState => userprofileState.user
  }
  const state = {
    user: {},
    error: null,
    isLoading: false,
    profilePic: require('@/assets/profile.png')
  }
  const store = {
    state,
    getters
  }
  return shallowMount(InvoiceCard, {store, localVue})
}

describe('invoiceList', () => {
  let wrapper
  beforeEach(() => {
    wrapper = myComponent()
  })
  it('should show the modal boxes', () => {
    expect(wrapper).toMatchSnapshot()
  })
  it('should test handle open modal when it has been clicked', () => {
    helperFunctions.openModal = jest.fn()
    wrapper.vm.handleOpenModalOnClick('invoice', 1)
    expect(helperFunctions.openModal).toHaveBeenCalledWith('invoice', 1)
  })
})
