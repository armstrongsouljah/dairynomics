import { shallowMount, createLocalVue } from '@vue/test-utils'
import UserDetails from '@/pages/Message/UserDetails'
import UserImage from '@/pages/Message/UserImage'
import Vuex from 'vuex'

const mockUser = {
  'id': '3564',
  'first_name': 'Kristoffer',
  'last_name': 'Stoltenberg',
  'email': 'kristofferstoltenberg1@testusers.cowsoko',
  'country': 'Uganda',
  'country_code': '+256',
  'phone': '0714433011',
  'account_type': '',
  'county': 'Boma',
  'profile': {
    'pic': 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg',
    'bio': null,
    'social': {
      'facebook': null,
      'twitter': null
    }
  },
  'status': 'Offline',
  'verified_account': 1,
  'is_seller': 0,
  'is_expert': 0
}

let getters
const localVue = createLocalVue()
localVue.use(Vuex)

describe('User details', () => {
  let wrapper
  beforeAll(() => {
    getters = {
      chatMate: () => mockUser
    }
    wrapper = shallowMount(UserDetails, {
      propsData: {
        toggleSideBar: jest.fn()
      },
      localVue,
      store: new Vuex.Store({ getters })
    })
  })

  it('renders without an error', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('renders with a child component', () => {
    expect(wrapper.find(UserImage).exists()).toBeTruthy()
  })

  it('renders with status set to offline', () => {
    expect(wrapper.vm.getStatusClasses).toBe('status offline')
  })
})
