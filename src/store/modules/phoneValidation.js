import axios from 'axios'

export default {
  state: {
    verification: '',
    verificationCode: '',
    token: '',
    erro: null,
    message: '',
    spinner: false
  },
  getters: {
    getterData: state => state
  },
  mutations: {
    GENERATED_TOKEN (state, verification, verificationCode) {
      state.verification = verification
      state.verificationCode = verificationCode
    },
    VALID_TOKEN (state, token) {
      state.token = token
    },
    INVALID_ERROR (state, error) {
      state.erro = error
    },
    SPIN_ICON (state, status) {
      state.spinner = status
    },
    REFRESH_ERROR (state) {
      state.erro = null
    },
    SEND_MESSAGE (state, message) {
      state.message = message
    }
  },
  actions: {
    async getRandomToken ({
      commit
    }, contactDetails) {
      axios.post('https://beta.cowsoko.com/api/v1/send_verification_code', {
        country_code: contactDetails.country_code,
        phone: contactDetails.phone
      })
        .then(function (response) {
          const verificationCode = response.data.verification_code
          commit('GENERATED_TOKEN', response.data, verificationCode)
        }).catch(error => error)
    },
    async verifyToken ({
      commit, rootState
    }, token) {
      commit('REFRESH_ERROR')
      commit('SPIN_ICON', true)
      await axios({
        url: `https://beta.cowsoko.com/api/v1/verify_code/${rootState.signup.userAuthSignUp.userId}`,
        method: 'put',
        data: {
          verification_code: token
        }
      })
        .then(function (response) {
          commit('VALID_TOKEN', response.data)
          commit('SPIN_ICON', false)
          commit('REFRESH_ERROR')
        })
        .catch(err => {
          commit('INVALID_ERROR', err.response.data)
          commit('SPIN_ICON', false)
        })
    },
    sendMessage ({
      commit, rootState, state
    }) {
      const PIN = state.verification.verification_code
      return axios({
        url: `https://beta.cowsoko.com/api/v1/send_sms/${rootState.signup.userAuthSignUp.userId}`,
        method: 'POST',
        data: {
          message: `Thank you for joining Dairynomics! Login to your account using ${PIN} and learn to become a better farmer! Click here www.dairynomicsapp.com`
        }
      })
        .then(function (response) {
          commit('SEND_MESSAGE', response.data)
        })
        .catch(err => {
          commit('INVALID_ERROR', err.response.data)
        })
    }
  }
}
