import Vuex from 'vuex'
import {shallowMount, createLocalVue} from '@vue/test-utils'
import index from '@/pages/Invoices/index'
import AllInvoicesModule from '@/store/modules/AllInvoicesModule'

const localVue = createLocalVue()
localVue.use(Vuex)
describe('Invoice Vue', () => {
  let wrapper
  const store = new Vuex.Store({modules: {AllInvoicesModule}})
  beforeEach(() => {
    wrapper = shallowMount(index, {store, localVue})
  })
  it('should test the spiner is shown when data is loaded', () => {
    expect(wrapper.vm.isLoading).toBe(true)
  })
  it('should test the get all invoice action is called when the compnent is created', () => {
    // wrapper.created()
  })
})
