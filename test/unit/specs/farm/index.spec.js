import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import FarmPage from '@/pages/Farm'

const localVue = createLocalVue()
localVue.use(Vuex)

const actions = {
  getFarms: jest.fn()
}

const getters = {
  getState: () => ({
    farms: [{name: 'joshua'}]
  })
}

const store = new Vuex.Store({
  getters,
  actions
})

const wrapper = shallowMount(FarmPage, {store, localVue})

describe('Farm Page Test', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('farmDetails computed value', () => {
    expect(wrapper.vm.farmDetails.length).toEqual(1)
  })
})
