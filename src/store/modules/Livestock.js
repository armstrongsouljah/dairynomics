import AxiosCalls from '@/utils/api/AxiosCalls'
import router from '@/router/router'

export default {
  state: {
    livestock: [],
    metaDetails: {},
    livestockTypes: [],
    livestockBreeds: [],
    livestockTypeSelected: 'All-Types',
    livestockBreedSelected: 'All-Breeds',
    isLoading: false
  },
  mutations: {
    SET_LIVESTOCK (state, payload) {
      const {livestock, livestocks} = payload.data
      state.livestock = livestock || livestocks
    },
    SET_LIVESTOCK_META_DETAILS (state, payload) {
      state.metaDetails = payload
    },
    SET_LIVESTOCK_META_DETAILS_ERROR (state) {
      state.metaDetails = {}
    },
    SET_LIVESTOCK_TYPES (state, payload) {
      state.livestockTypes = [
        state.livestockTypeSelected,
        ...payload.livestock_type
      ]
    },
    SET_LIVESTOCK_BREEDS (state, payload) {
      state.livestockBreeds = [
        state.livestockBreedSelected,
        ...payload.livestock_type
      ]
    },
    SET_LIVESTOCK_BREEDS_ERROR (state) {
      state.livestockBreeds = []
    },
    SET_LIVESTOCK_ERROR (state) {
      state.livestock = []
    },
    TOGGLE_IS_LOADING (state, payload) {
      state.isLoading = payload
    }
  },
  actions: {
    FETCH_LIVESTOCK: async (context, page) => {
      context.commit('TOGGLE_IS_LOADING', true)
      const token = localStorage.getItem('token')
      try {
        let {data} = await AxiosCalls.get(
          `api/v1/livestocks?page=${page || 1}`,
          token
        )
        context.commit('SET_LIVESTOCK', data)
        context.commit('SET_LIVESTOCK_META_DETAILS', data.meta)
        context.commit('TOGGLE_IS_LOADING', false)
      } catch (error) {
        context.commit('SET_LIVESTOCK_ERROR')
        context.commit('TOGGLE_IS_LOADING', false)
        switch (error.response.status) {
          case 404:
            return router.push('/404')
          default:
            return router.push('/')
        }
      }
    },
    FETCH_LIVESTOCK_TYPES: async context => {
      context.commit('TOGGLE_IS_LOADING', true)
      const token = localStorage.getItem('token')
      try {
        let {data} = await AxiosCalls.get('api/v1/livestock_type', token)
        context.commit('SET_LIVESTOCK_TYPES', data)
        context.commit('TOGGLE_IS_LOADING', false)
      } catch (error) {
        context.commit('SET_LIVESTOCK_TYPES_ERROR')
        context.commit('TOGGLE_IS_LOADING', false)
        switch (error.response.status) {
          case 404:
            return router.push('/404')
          default:
            return router.push('/')
        }
      }
    },
    FETCH_LIVESTOCK_BREEDS: async (context, livestockType) => {
      context.commit('TOGGLE_IS_LOADING', true)
      const token = localStorage.getItem('token')
      try {
        let {data} = await AxiosCalls.get(
          `api/v1/livestock_breed/${livestockType}`,
          token
        )
        context.commit('SET_LIVESTOCK_BREEDS', data || {livestock_type: []})
        context.commit('TOGGLE_IS_LOADING', false)
      } catch (error) {
        context.commit('SET_LIVESTOCK_BREEDS_ERROR')
        context.commit('TOGGLE_IS_LOADING', false)
        switch (error.response.status) {
          case 404:
            return router.push('/404')
          default:
            return router.push('/')
        }
      }
    },
    FILTER_LIVESTOCK: async ({commit}, {filteredBy, page, expertId = ''}) => {
      let url = expertId ? `api/v1/experts/${expertId}/livestocks` : 'api/v1/livestock'
      commit('TOGGLE_IS_LOADING', true)
      const token = localStorage.getItem('token')
      let filterString = ''

      Object.keys(filteredBy).forEach(key => {
        if (filteredBy[key] !== '') {
          if (filterString === '') {
            filterString += `filter[${key}]=${filteredBy[key]}`
          } else {
            filterString += `&filter[${key}]=${filteredBy[key]}`
          }
        }
      })

      if (filterString !== '') {
        url += `?${filterString}&page[number]=${page || 1}`
      } else {
        url += `?page[number]=${page || 1}`
      }

      try {
        let {data} = await AxiosCalls.get(url, token)
        commit('SET_LIVESTOCK', data)
        commit('SET_LIVESTOCK_META_DETAILS', data.meta)
        commit('TOGGLE_IS_LOADING', false)
      } catch (error) {
        commit('SET_LIVESTOCK_ERROR')
        commit('SET_LIVESTOCK_META_DETAILS_ERROR')
        commit('TOGGLE_IS_LOADING', false)
        switch (error.response.status) {
          case 404:
            return router.push('/404')
          default:
            return router.push('/')
        }
      }
    },
    FETCH_EXPERT_LIVESTOCK: async (context, expertId) => {
      context.commit('TOGGLE_IS_LOADING', true)
      const token = localStorage.getItem('token')
      context.commit('SET_LIVESTOCK', {data: {livestock: []}})
      try {
        let {data} = await AxiosCalls.get(
          `api/v1/experts/${expertId}/livestocks`,
          token
        )
        if (data.success) {
          context.commit('SET_LIVESTOCK', data)
          context.commit('SET_LIVESTOCK_META_DETAILS', data.meta)
          context.commit('TOGGLE_IS_LOADING', false)
        } else {
          context.commit('SET_FARMS_ERROR', 'No content found', {root: true})
          context.commit('TOGGLE_IS_LOADING', false)
        }
      } catch (error) {
        switch (error.response.status) {
          case 404:
            return router.push('/404')
          default:
            return router.push('/')
        }
      }
    }
  }
}
