import { openModal } from '@/utils/helperFunctions'

export default {
  methods: {
    showSignupModal () {
      openModal('signup')
    }
  }
}
