import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import ServiceProviderUpdateIndex from '@/pages/ServiceProviderUpdates/index.vue'
import Switcher from '@/components/ui/Switcher.vue'
import topics from '@/store/modules/topics.js'
import expert from '@/store/modules/expert.js'

const localVue = createLocalVue()

localVue.use(Vuex)

// jest.mock('@/router/router')

jest.mock('@/store')

describe('ServiceProviderUpdateIndexPage', () => {
  let store
  let actions
  let state
  let wrapper

  beforeEach(() => {
    Storage.prototype.getItem = jest.fn(() => 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEyMzQsIm5hbWUiOiJKb2huIERvZSJ9.QSEniO93_dA_wKUCEQ2Mjk06QwTDfHx_DqdfzilYU78')
    actions = {
      GET_TOPICS: jest.fn(),
      GET_EXPERT_UPDATES: jest.fn()
    }

    state = {
      topics: [],
      expertUpdates: []
    }

    store = new Vuex.Store({
      modules: {
        topics: {
          state,
          getters: topics.getters,
          actions,
          mutations: topics.mutations
        },
        expert: {
          state: expert.state,
          getters: expert.getters,
          actions,
          mutations: expert.mutations
        }
      }
    })
    wrapper = shallowMount(ServiceProviderUpdateIndex, { store, localVue })
  })
  it('should dispatch GET_TOPICS action when component is mounted', () => {
    expect(actions.GET_TOPICS).toHaveBeenCalled()
  })
  it('should dispatch GET_EXPERT_UPDATES action when component is mounted', () => {
    expect(actions.GET_EXPERT_UPDATES).toHaveBeenCalled()
  })
  it('changes activeLink when the changeActiveLinkStatus method is triggered', () => {
    wrapper.setData({ activeLink: 'myUpdates' })
    wrapper.vm.changeActiveLinkStatus('myProfile')
    expect(wrapper.vm.activeLink).toBe('myProfile')
  })
  it('triggers the changeActiveLinkStatus method when @changeActiveLinkStatus is emitted from the Switcher component', () => {
    const mock = jest.fn()
    wrapper.setMethods({ changeActiveLinkStatus: mock })
    wrapper.find(Switcher).vm.$emit('changeActiveLinkStatus')
    expect(mock).toHaveBeenCalled()
  })
})
