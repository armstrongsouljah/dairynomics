import AxiosCalls from '@/utils/api/AxiosCalls'
import getRandomItem from '@/utils/performance/getRandomItem'
import router from '@/router/router'

export default {
  state: {
    livestock: [],
    randomLivestock: {}
  },
  getters: {
    USER_LIVESTOCK (state) {
      return state.livestock
    },
    RANDOM_USER_LIVESTOCK (state) {
      return state.randomLivestock
    }
  },
  mutations: {
    SET_USER_LIVESTOCK (state, livestock) {
      state.livestock = livestock
    },
    SET_RANDOM_USER_LIVESTOCK (state, payload) {
      state.randomLivestock = payload
    }
  },
  actions: {
    GET_USER_LIVESTOCK: ({commit}) => {
      return AxiosCalls.get('api/v1/user_livestocks')
        .then((response) => {
          const randomLiveStock = getRandomItem(response.data.data.livestocks)
          commit('SET_USER_LIVESTOCK', response.data.data.livestocks)
          commit('SET_RANDOM_USER_LIVESTOCK', randomLiveStock)
        })
        .catch((error) => {
          switch (error.response.status) {
            case 404:
              return router.push('/404')
            default:
              return router.push('/')
          }
        })
    }
  }
}
