import { shallowMount } from '@vue/test-utils'
import BlogCommentsPage from '@/pages/BlogCommentsPage'

describe('BlogCommentsPage test', () => {
  const wrapper = shallowMount(BlogCommentsPage, {})

  it('it should render the required html', () => {
    expect(wrapper.contains('div')).toBe(true)
  })
})
