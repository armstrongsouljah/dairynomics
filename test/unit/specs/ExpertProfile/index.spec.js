import { shallowMount, createLocalVue } from '@vue/test-utils'
import ExpertProfileIndex from '@/pages/ExpertProfilePage/index.vue'
import UserProfileSwitcher from '@/pages/ExpertProfilePage/Switcher.vue'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)
describe('index page for user profile timeline', () => {
  let getters
  let store
  let wrapper
  getters = {
    getExpertProfileInformation: () => {
      return { getExpertProfile: {profile: {pic: ''}},
        ExpertProfileError: null}
    }}
  store = new Vuex.Store({getters})
  wrapper = shallowMount(ExpertProfileIndex, { store,
    localVue})

  it('renders the UserProfileSwticher component on load', () => {
    expect(wrapper.find(UserProfileSwitcher).exists()).toBe(true)
  })

  it('renders the UserProfileTimeline component on page load default', () => {
    expect(wrapper.find(ExpertProfileIndex).exists()).toBe(true)
    wrapper.find(ExpertProfileIndex).vm.$emit('changeProfileSection')
  })
})
