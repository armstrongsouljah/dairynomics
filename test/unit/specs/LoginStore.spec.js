import axios from 'axios'
import LoginStore from '@/store/modules/LoginStore'
import AxiosMockAdapter from 'axios-mock-adapter'
import router from '@/router/router'

const mockState = {
  loggingIn: false,
  loginError: null,
  loginSuccessful: false,
  authenticationError: 'there is error',
  userId: null,
  isLoggedIn: false
}

const mock = new AxiosMockAdapter(axios)
jest.mock('@/router/router')

describe('LoginStore', () => {
  describe('loginStart mutation', () => {
    it('authenticated a user', () => {
      const state = {}
      let countryCode = '+256'
      let phone = '0754063403'
      let password = '1234'
      LoginStore.mutations.loginStart(state, {countryCode, phone, password})
      expect(state).toEqual({'loggingIn': true})
    })
  })
  describe('loginStop mutation', () => {
    it('wrong information', () => {
      const state = {}
      let countryCode = '+256'
      let phone = '0754063403'
      let password = '1234'
      LoginStore.mutations.loginStop(state, {countryCode, phone, password})
      expect(state).toEqual({'loggingIn': false, 'loginError': {'countryCode': '+256', 'password': '1234', 'phone': '0754063403'}, 'loginSuccessful': false})
    })
  })
  describe('login action', () => {
    it('Login  a user', () => {
      const commit = jest.fn()
      router.push = jest.fn()
      const resp = {
        success: true,
        phone: '0754063403',
        password: '1234',
        country_code: '+256',
        user: {
          id: 45
        }
      }
      mock.onPost('https://beta.cowsoko.com/api/v1/login').reply(200, resp)
      LoginStore.actions.doLogin({commit}, 'loginData')
        .then(() => {
          expect(commit).toHaveBeenCalled()
          expect(router.push).toHaveBeenCalled()
        })
    })

    it('Login  a user when currentRoute.path is not "/" ', () => {
      const commit = jest.fn()
      router.push = jest.fn()
      router.currentRoute.fullPath = '/joshua'
      const resp = {
        success: true,
        phone: '0754063403',
        password: '1234',
        country_code: '+256',
        user: {
          id: 45
        }
      }
      mock.onPost('https://beta.cowsoko.com/api/v1/login').reply(200, resp)
      LoginStore.actions.doLogin({commit}, 'loginData')
        .then(() => {
          expect(commit).toHaveBeenCalled()
          expect(router.push).toHaveBeenCalled()
        })
    })

    it('Login  a user', () => {
      const commit = jest.fn()
      const resp = {
        success: false
      }
      mock.onPost('https://beta.cowsoko.com/api/v1/login').reply(200, resp)
      LoginStore.actions.doLogin({commit}, 'loginData')
      expect(commit).toHaveBeenCalled()
    })
  })
  it('test for the login user', () => {
    const state = {loggingIn: false,
      loginError: null,
      loginSuccessful: false}
    LoginStore.getters.LoginDetails(state)
    expect(state).toEqual(state)
  })

  it('Login  a user fails with wrong phone or password', () => {
    const commit = jest.fn()
    const resp = {
      message: 'Wrong phone number or password'
    }
    mock.onPost('https://beta.cowsoko.com/api/v1/login').reply(422, resp)
    LoginStore.actions.doLogin({commit}, resp)
    expect(commit).toHaveBeenCalled()
  })

  it('Login  a user fails with Account number not verified', () => {
    const commit = jest.fn()
    const resp = {
      message: 'Account Not Verified'
    }
    mock.onPost('https://beta.cowsoko.com/api/v1/login').reply(422, resp)
    LoginStore.actions.doLogin({commit}, resp)
    expect(commit).toHaveBeenCalled()
  })

  it('Login  a user fails with phone number not verified', () => {
    const commit = jest.fn()
    const resp = {
      error: 'phone number invalid'
    }
    mock.onPost('https://beta.cowsoko.com/api/v1/login').reply(422, resp)
    LoginStore.actions.doLogin({commit}, resp)
    expect(commit).toHaveBeenCalled()
  })

  it('Login  a user fails with any other kind of error', () => {
    const commit = jest.fn()
    const resp = {
      error: 'Joshua'
    }
    mock.onPost('https://beta.cowsoko.com/api/v1/login').reply(422, resp)
    LoginStore.actions.doLogin({commit}, resp)
    expect(commit).toHaveBeenCalled()
  })

  it('should set mutations toggleIsLoggedInState', () => {
    LoginStore.mutations.toggleIsLoggedInState(LoginStore.state, true)
    expect(LoginStore.state.isLoggedIn).toBeTruthy()
  })

  it('should set mutations set AuthError', () => {
    LoginStore.mutations.setAuthError(LoginStore.state, 'joshua')
    expect(LoginStore.state.authenticationError).toBe('joshua')
  })

  it('isLogged in getter should return isLoggedInStatus', () => {
    const expectedStatus = LoginStore.getters.isLoggedIn(mockState)
    expect(expectedStatus.status).toBeFalsy()
  })

  it('auth Error getter should return authentication Error', () => {
    const expectedError = LoginStore.getters.authError(mockState)
    expect(expectedError.error).toEqual('there is error')
  })
})
