import {createLocalVue, shallowMount} from '@vue/test-utils'
import SaleProduct from '@/components/ui/SaleProduct'

const localVue = createLocalVue()

const propsData = {
  id: 23,
  price: 250,
  image_src: 'https://joshua.jpg',
  product_name: 'joshua',
  county: 'joshua'
}
const wrapper = shallowMount(SaleProduct, {localVue, propsData})
describe('SaleProduct.Vue test', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })
})
