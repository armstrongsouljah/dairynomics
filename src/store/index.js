import Vue from 'vue'
import Vuex from 'vuex'
import LostPinModule from './modules/LostPinModule'
import signupStoreModules from './modules/signupStoreModules'
import LoginStore from './modules/LoginStore'
import phoneValidation from './modules/phoneValidation'
import blogs from './modules/blogs'
import landingPage from './modules/landingPage'
import notificationsModule from './modules/notificationsModule'
import UpdateProfileStore from './modules/UpdateProfileStore'
import uploadProductStore from './modules/uploadProductStore'
import uploadInputStore from './modules/uploadInputStore'
import uploadShareImageStore from './modules/uploadShareImageStore'
import individualLivestock from './modules/individualLivestock'
import individualFarm from './modules/individualFarm'
import UserProfileModule from './modules/UserProfileModule'
import ExpertProfileModule from './modules/ExpertProfileModule'
import UserLivestockModule from './modules/UserLivestockModule'
import UpdateModule from './modules/UpdatesModule'
import UserPublicationModule from './modules/UserPublicationModule'
import expertList from './modules/expertList'
import UserTestsModule from './modules/UserTestsModule'
import Farms from './modules/Farm'
import publicationsModule from './modules/publicationsModule'
import IndivualPublicationModule from './modules/IndivualPublicationModule'
import ExpertUserProfileModule from './modules/ExpertUserProfileModule'
import Livestock from './modules/Livestock'
import IndividualProductModule from './modules/IndividualProductModule'
import productsModule from './modules/productsModule'
import AllBlogs from './modules/AllBlogs'
import SingleUpdateModule from './modules/SingleUpdateModule'
import allUpdatesModule from './modules/allUpdatesModule'
import topics from '@/store/modules/topics'
import expert from '@/store/modules/expert'
import FarmUpdates from '@/store/modules/FarmUpdatesModule'
import PostCommentModule from './modules/PostCommentModule'
import messageModule from '@/store/modules/messageModule'
import UpgradeSubscription from './modules/UpgradeSubscription'
import AllReceipts from './modules/AllReceipts'
import IndividualReceipt from './modules/IndividualReceipt'
import AllInvoicesModule from './modules/AllInvoicesModule'

import subscriptionModule from './modules/subscription'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    lostpin: LostPinModule,
    signup: signupStoreModules,
    LoginStore,
    phoneValidation,
    blogs,
    landingPage,
    notificationsModule,
    UpdateProfileStore,
    individualLivestock,
    ExpertProfile: ExpertProfileModule,
    UserLivestock: UserLivestockModule,
    UpdateModule,
    UserPublicationModule,
    UserTestsModule,
    Farms,
    individualFarm,
    expertList,
    IndivualPublicationModule,
    publicationsModule,
    ExpertUserProfileModule,
    Livestock,
    IndividualProductModule,
    UserProfileModule,
    productsModule,
    AllBlogs,
    topics,
    SingleUpdateModule,
    allUpdatesModule,
    expert,
    uploadProductStore,
    uploadInputStore,
    uploadShareImageStore,
    FarmUpdates,
    PostCommentModule,
    messageModule,
    UpgradeSubscription,
    subscriptionModule,
    AllReceipts,
    IndividualReceipt,
    AllInvoicesModule
  }
})
