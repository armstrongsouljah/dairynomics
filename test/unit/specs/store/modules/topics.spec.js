import { createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import topics from '@/store/modules/topics.js'

const localVue = createLocalVue()

localVue.use(Vuex)

const mock = new MockAdapter(axios)

jest.mock('@/store')

describe('Topics module', () => {
  let state

  beforeEach(() => {
    state = {
      topics: []
    }
  })

  it('COMMITS a SET_TOPICS mutation when component is mounted', (done) => {
    const context = {
      commit: jest.fn()
    }

    const payload = {
      token: '2wudwuuwu388598gjvdklsl'
    }

    mock.onGet(`https://beta.cowsoko.com/api/v1/topics`).reply(201, {
      data: {
        data: { experts: [
          { id: 1, topic: 'Cow Health and Maternity', body: 'Test body' }
        ]
        }
      }
    })

    topics.actions.GET_TOPICS(context, payload).then(() => {
      expect(context.commit).toHaveBeenCalledTimes(1)
      done()
    })
  })
  it('mutates SET_TOPICS when component is mounted', (done) => {
    const payload = {
      topics: [{
        name: 'Cow herding',
        id: 1
      }]
    }

    topics.mutations.SET_TOPICS(state, payload)
    expect(state.topics).toEqual(payload.topics)
    done()
  })
})
