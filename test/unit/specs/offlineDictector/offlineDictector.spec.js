import { shallowMount, createLocalVue } from '@vue/test-utils'
import OfflineIndicator from '@/components/ui/OfflineIndicator'

const localVue = createLocalVue()

describe('offlineIndicator', () => {
  let wrapper = shallowMount(OfflineIndicator, { localVue })

  it('should trigger the handleConnectivityChange method', () => {
    const connectivitySpy = jest.spyOn(wrapper.vm, 'handleConnectivityChange')
    connectivitySpy(true)
    expect(wrapper.vm.online).toEqual(true)
    expect(wrapper.vm.showMessage).toBeFalsy()
  })

  it('should call setTimeout and change showMessaget to false', () => {
    wrapper.setData({online: false})
    expect(wrapper.vm.showMessage).toBeTruthy()
  })
})
