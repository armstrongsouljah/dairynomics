import { shallowMount } from '@vue/test-utils'
import SelectField from '@/components/ui/SelectField.vue'

describe('@components/ui/Switcher.vue', () => {
  const propsObject = {
    propsData: {
      options: [{
        id: 1,
        topic: 'Cow Herding'
      }],
      id: 'my-select-field'
    }
  }
  const wrapper = shallowMount(SelectField, propsObject)
  it('emits a "handleSelectFieldChange" event when a non default option is selected', () => {
    wrapper.find('#my-select-field').trigger('change')
    expect(wrapper.emitted('handleSelectFieldChange').length).toEqual(1)
  })
})
