import {createLocalVue, shallowMount} from '@vue/test-utils'
import VueRouter from 'vue-router'

import UpdateCard from '@/components/ui/UpdateCard.vue'

const localVue = createLocalVue()
localVue.use(VueRouter)

const propsData = {
  id: '34',
  blogs: [],
  blogType: 'joshua',
  completedReading: true,
  numberOfExpertUpdates: 23,
  blog: {
    photo: ['https://joshua.jpg', 'joshua.jpg'],
    id: 23,
    text: 'Blog',
    likes: 23,
    number_of_comments: 23,
    user: {
      first_name: 'Joshua',
      county: 'joshua',
      number_of_updates: 23,
      id: 34,
      profile: {
        pic: 'https://joshua.jpg'
      }
    }}
}
const wrapper = shallowMount(UpdateCard, {propsData, localVue})

describe('UpdateCard test', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('whatsAppShareText test', () => {
    expect(wrapper.vm.whatsAppShareText).toBeTruthy()
  })

  it('checkIfhostedImages', () => {
    expect(wrapper.vm.checkIfHostedImages).toBeTruthy()
  })

  it('checkIfhostedImages', () => {
    const newBlogs = {
      blog: {
        photo: [],
        id: 23,
        text: 'Blog',
        likes: 23,
        number_of_comments: 23,
        user: {
          first_name: 'Joshua',
          county: 'joshua',
          number_of_updates: 23,
          id: 34,
          profile: {
            pic: 'https://joshua.jpg'
          }
        }}
    }
    wrapper.setProps({blog: newBlogs.blog})
    expect(wrapper.vm.checkIfHostedImages).toBeFalsy()
  })

  it('checkIfhostedImages', () => {
    const newBlogs = {
      blog: {
        photo: ['https://beta.cowsoko.com/storage/img'],
        id: 23,
        text: 'Blog',
        likes: 23,
        number_of_comments: 23,
        user: {
          first_name: 'Joshua',
          county: 'joshua',
          number_of_updates: 23,
          id: 34,
          profile: {
            pic: 'https://joshua.jpg'
          }
        }}
    }
    wrapper.setProps({blog: newBlogs.blog})
    expect(wrapper.vm.checkIfHostedImages).toBeTruthy()
  })

  it('checkIfhostedImages', () => {
    const newBlogs = {
      blog: {
        photo: [''],
        id: 23,
        text: 'Blog',
        likes: 23,
        number_of_comments: 23,
        user: {
          first_name: 'Joshua',
          county: 'joshua',
          number_of_updates: 23,
          id: 34,
          profile: {
            pic: 'https://joshua.jpg'
          }
        }}
    }
    wrapper.setProps({blog: newBlogs.blog})
    expect(wrapper.vm.checkIfHostedImages).toBeFalsy()
  })
})
