import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import AppNavigationBar from '@/components/layout/AppNavigationBar.vue'
import DropDown from '@/components/ui/DropDown.vue'
import PeerModel from '@/utils/peerModel/index.js'
import VueRouter from 'vue-router'

let localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

const mockUsers = [{
  'id': '3564',
  'first_name': 'Kristoffer',
  'last_name': 'Stoltenberg',
  'email': 'kristofferstoltenberg1@testusers.cowsoko',
  'country': 'Uganda',
  'country_code': '+256',
  'phone': '0714433011',
  'account_type': '',
  'county': 'Boma',
  'profile': {
    'pic': 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg',
    'bio': null,
    'social': {
      'facebook': null,
      'twitter': null
    }
  },
  'unread_messages_count': 0,
  'verified_account': 1,
  'is_seller': 0,
  'is_expert': 0
}, {
  id: '3554',
  'first_name': 'Chris',
  'last_name': 'Stoneberg',
  'email': 'kristofferstoltenberg1@testusers.cowsoko',
  'country': 'Uganda',
  'country_code': '+256',
  'phone': '0714433011',
  'account_type': '',
  'county': 'Boma',
  'profile': {
    'pic': 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg',
    'bio': null,
    'social': {
      'facebook': null,
      'twitter': null
    }
  },
  'unread_messages_count': 0,
  'verified_account': 1,
  'is_seller': 0,
  'is_expert': 0
}]

describe('AppNavigationBar', () => {
  let actions
  let getters
  let store
  let wrapper

  describe('not signed in user', () => {
    beforeAll(() => {
      const state = {
        users: mockUsers,
        isLoggedIn: false
      }
      actions = {
        fetchNotifications: jest.fn(),
        markAsRead: jest.fn(),
        GET_USER_PROFILE: jest.fn()
      }
      getters = {
        allUsers: () => mockUsers,
        PROFILE_PIC: () => '',
        hasNewMessage: () => false,
        peerModel: () => new PeerModel(),
        isLoggedIn (state) {
          return { status: state.isLoggedIn }
        },
        chatMate: () => mockUsers[0],
        USER_PROFILE: () => ({
          ...mockUsers[0],
          chat_stats: {
            all_messages_count: 0
          }
        }),
        getNotifications: () => ({ notifications: [] })
      }
      store = new Vuex.Store({ state, actions, getters })
      wrapper = shallowMount(AppNavigationBar, {
        propsData: {
          users: mockUsers
        },
        store,
        localVue,
        stubs: ['router-link']
      })
    })

    it('renders correctly', () => {
      expect(wrapper.isVueInstance()).toBeTruthy()
      expect(wrapper.vm.userDropdownMenu).toHaveLength(2)
    })

    it('should return false if use has unread messages', () => {
      expect(wrapper.vm.hasUnreadMessages).toBeFalsy()
    })

    it('should return false if use has messages', () => {
      expect(wrapper.vm.hasMessages()).toBeFalsy()
    })

    it('should load messages', () => {
      const loadMessagePageSpy = jest.spyOn(wrapper.vm, 'loadMessagePage')
      loadMessagePageSpy()
      expect(wrapper.vm.loadMessagePage).toHaveBeenCalled()
    })

    it('should not call connectPeer', () => {
      const connectPeerSpy = jest.spyOn(wrapper.vm, 'connectPeer')
      wrapper.setData({
        users: []
      })
      expect(connectPeerSpy).not.toHaveBeenCalled()
    })

    it('should call connectPeer', () => {
      const connectPeerSpy = jest.spyOn(wrapper.vm, 'connectPeer')
      wrapper.setData({
        users: [mockUsers[0]]
      })
      expect(connectPeerSpy).toHaveBeenCalled()
    })

    it('should not call "fetchNotification"', () => {
      expect(actions.fetchNotifications).not.toHaveBeenCalled()
    })

    it('should not show notification and message icons', () => {
      expect(wrapper.contains(DropDown)).toBe(false)
      expect(wrapper.findAll('.nav-item')).toHaveLength(1)
    })

    it('should call "fetchNotification" when user logs in', () => {
      store.state.isLoggedIn = true
      expect(actions.fetchNotifications).toHaveBeenCalledTimes(1)
    })
  })

  describe('signed in user', () => {
    describe('user without new notification', () => {
      beforeAll(() => {
        actions = {
          fetchNotifications: jest.fn(),
          markAsRead: jest.fn(),
          GET_USER_PROFILE: jest.fn()
        }
        getters = {
          PROFILE_PIC: () => '',
          isLoggedIn: () => ({ status: true }),
          getNotifications: () => ({
            notifications: [
              { viewed: '1' }
            ]
          }),
          USER_PROFILE: () => ({
            ...mockUsers[0],
            chat_stats: {
              all_messages_count: 0
            }
          })
        }
        store = new Vuex.Store({ actions, getters })
        wrapper = shallowMount(AppNavigationBar, {
          store,
          localVue,
          stubs: ['router-link']
        })
      })

      it('renders correctly', () => {
        expect(wrapper.isVueInstance()).toBeTruthy()
        expect(wrapper.vm.userDropdownMenu).toHaveLength(3)
      })

      it('should call "fetchNotification"', () => {
        expect(actions.fetchNotifications).toHaveBeenCalledTimes(1)
      })

      it('should show notification and message icons', () => {
        expect(wrapper.contains(DropDown)).toBe(false)
      })

      it('should not show new notification icon', () => {
        expect(wrapper.findAll('.new-notification')).toHaveLength(0)
      })

      it('should show "DropDown" on clicking "bell icon"', () => {
        wrapper.find('.bell-nav-icon-link').trigger('click')
        expect(wrapper.contains(DropDown)).toBe(true)
      })

      it('should toggle "DropDown" to not show on clicking "bell icon" again', () => {
        wrapper.find('.bell-nav-icon-link').trigger('click')
        expect(wrapper.contains(DropDown)).toBe(false)
      })
    })

    describe('user with new notification', () => {
      beforeAll(() => {
        actions = {
          fetchNotifications: jest.fn(),
          markAsRead: jest.fn(),
          GET_USER_PROFILE: jest.fn()
        }
        getters = {
          PROFILE_PIC: () => '',
          isLoggedIn: () => ({ status: true }),
          getNotifications: () => ({
            notifications: [
              { viewed: '1', 'parent_reference': null },
              { viewed: '0', 'parent_reference': 1 }
            ]
          }),
          USER_PROFILE: () => ({
            ...mockUsers[0],
            chat_stats: {
              all_messages_count: 0
            }
          })
        }
        store = new Vuex.Store({ actions, getters })
        wrapper = shallowMount(AppNavigationBar, { store, localVue, stubs: ['router-link'] })
      })

      it('renders correctly', () => {
        expect(wrapper.isVueInstance()).toBeTruthy()
        expect(wrapper.vm.userDropdownMenu).toHaveLength(3)
      })

      it('should call "fetchNotification"', () => {
        expect(actions.fetchNotifications).toHaveBeenCalledTimes(1)
      })

      it('should show notification and message icons', () => {
        expect(wrapper.contains(DropDown)).toBe(false)
      })

      it('should show new notification icon', () => {
        expect(wrapper.findAll('.new-notification')).toHaveLength(1)
      })

      it('should show "DropDown" and call "markAsRead on clicking "bell icon"', () => {
        wrapper.find('.bell-nav-icon-link').trigger('click')
        expect(wrapper.contains(DropDown)).toBe(true)
        expect(actions.markAsRead).toHaveBeenCalledTimes(1)
      })
    })

    describe('user with no notification', () => {
      beforeAll(() => {
        actions = {
          fetchNotifications: jest.fn(),
          markAsRead: jest.fn(),
          GET_USER_PROFILE: jest.fn()
        }
        getters = {
          PROFILE_PIC: () => '',
          isLoggedIn: () => ({ status: true }),
          getNotifications: () => ({
            notifications: [

            ]
          }),
          USER_PROFILE: () => ({
            ...mockUsers[0],
            chat_stats: {
              all_messages_count: 0
            }
          })
        }
        store = new Vuex.Store({ actions, getters })
        wrapper = shallowMount(AppNavigationBar, { store, localVue, stubs: ['router-link'] })
      })

      it('renders correctly', () => {
        expect(wrapper.isVueInstance()).toBeTruthy()
        expect(wrapper.vm.userDropdownMenu).toHaveLength(3)
      })

      it('should call "fetchNotification"', () => {
        expect(actions.fetchNotifications).toHaveBeenCalledTimes(1)
      })

      it('should show notification and message icons', () => {
        expect(wrapper.contains(DropDown)).toBe(false)
        expect(wrapper.findAll('.nav-item')).toHaveLength(2)
      })

      it('should show DropDown with "You have no notifications yet" message', () => {
        wrapper.find('.bell-nav-icon-link').trigger('click')
        expect(wrapper.contains(DropDown)).toBe(true)
      })
    })
  })
})
