import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import IndividualProductHeader from '@/pages/IndividualProduct/IndividualProductHeader.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
let store

describe('IndividualProductHeader', () => {
  let getters
  beforeEach(() => {
    getters = {
      getIndividualProduct: () => {
        return {
          IndividualProductDetails: {}
        }
      }
    }
    store = new Vuex.Store({getters
    })
  })

  it('should render the IndividualProductHeader component', () => {
    let wrapper = shallowMount(IndividualProductHeader, {store, localVue})
    expect(wrapper.vm).toBeTruthy()
  })
})
