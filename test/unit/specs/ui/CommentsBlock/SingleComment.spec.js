import { mount, shallowMount, createLocalVue } from '@vue/test-utils'
import SingleComment from '@/components/ui/CommentsBlock/SingleComment.vue'
import moment from 'vue-moment'

let wrapper
const localVue = createLocalVue()
localVue.use(moment)

const $parent = {
  deleteComment: jest.fn()
}
describe('@components/ui/CommentsBlock/SingleComment.vue', () => {
  beforeEach(() => {
    wrapper = mount(SingleComment, {
      localVue,
      propsData: {
        comment: {
          comment: 'Comment',
          created_at: {
            date: '2019-06-04 16:39:01.000000',
            timezone_type: 3,
            timezone: 'Africa/Nairobi'
          },
          created_by: {
            id: '1',
            first_name: 'First Name',
            last_name: 'Last Name'
          },
          id: '2',
          likes: 0,
          number_of_replies: 0,
          replies: [],
          updated_at: {
            date: '2019-06-04 16:39:01.000000',
            timezone_type: 3,
            timezone: 'Africa/Nairobi'
          },
          user_likes: []
        },
        userId: '1'
      }
    })
    wrapper.vm.$parent.deleteComment = jest.fn()
  })

  it('should render with default props', () => {
    wrapper = shallowMount(SingleComment, {
      localVue,
      mocks: {
        $parent
      }
    })
    expect(wrapper.props().comment).toEqual({
      created_at: {
        date: '2019-01-01'
      }
    })
  })
  it('contains the `delete button`', () => {
    expect(wrapper.contains('.btn-delete')).toBe(true)
  })

  it('contains the `edit button`', () => {
    expect(wrapper.contains('.btn-edit')).toBe(true)
  })

  it('should change `editing state` when `edit button` is clicked', () => {
    wrapper.find('.btn-edit').trigger('click')
    expect(wrapper.vm.editing).toBeTruthy()
  })

  it('should change `replying state` when `reply button` is clicked', () => {
    wrapper.find('.btn-reply').trigger('click')
    expect(wrapper.vm.replying).toBeTruthy()
  })

  it('should call parent `toggleReplying` when `reply button` is clicked', () => {
    wrapper.setProps({
      isReply: true
    })
    wrapper.vm.$parent.toggleReplying = jest.fn()
    wrapper.find('.btn-reply').trigger('click')
    expect(wrapper.props().isReply).toBeTruthy()
  })

  it('should call parent `deleteComment` when `delete button` is clicked', () => {
    const onDelete = jest.fn()
    wrapper.vm.onDelete = onDelete
    wrapper.find('.btn-delete').trigger('click')
    expect(onDelete).toHaveBeenCalled()
  })

  it('should call parent `deleteComment` for reply when `delete button` is clicked', () => {
    wrapper.setProps({
      isReply: true
    })
    const onDelete = jest.fn()
    wrapper.vm.onDelete = onDelete
    wrapper.find('.btn-delete').trigger('click')
    expect(onDelete).toHaveBeenCalled()
  })

  it('should call parent `likeComment` when `like button` is clicked', () => {
    const likeComment = jest.fn()
    wrapper.vm.$parent.likeComment = likeComment
    wrapper.find('.btn-like').trigger('click')
    expect(likeComment).toHaveBeenCalled()
  })

  it('should call parent `likeComment` for reply when `like button` is clicked', () => {
    wrapper.setProps({
      isReply: true
    })
    const likeComment = jest.fn()
    wrapper.vm.$parent.likeComment = likeComment
    wrapper.find('.btn-like').trigger('click')
    expect(likeComment).toHaveBeenCalled()
  })

  it('should call `cancelComment` method when `cancel button` is clicked', () => {
    wrapper.setData({
      replying: true
    })
    wrapper.vm.$children[0].body = 'comment'
    wrapper.find('.cancel-button').trigger('click')
    expect(wrapper.vm.replying).toBeFalsy()
  })

  it('should call `submitComment` method when `submit button` is clicked', () => {
    wrapper.setData({
      replying: true
    })
    wrapper.vm.$children[0].body = 'comment'
    wrapper.find('.post-button').trigger('click')
  })

  it('should call `editComment` method when `post button` is clicked', () => {
    wrapper.setData({
      editing: true
    })
    const editComment = jest.fn()
    wrapper.vm.$children[0].body = 'comment'
    wrapper.vm.$parent.editComment = editComment
    wrapper.find('.post-button').trigger('click')
    expect(editComment).toHaveBeenCalled()
  })

  it('deleteComment', () => {
    wrapper.setProps({
      isReply: true
    })

    const deleteCommentSpy = jest.spyOn(wrapper.vm, 'deleteComment')
    deleteCommentSpy('joshua', 'david')
    expect(wrapper.vm.deleting).toBeFalsy()
  })

  it('deleteComment', () => {
    wrapper.setProps({
      isReply: false
    })
    wrapper.vm.$parent.deleteComment = jest.fn()
    const deleteCommentSpy = jest.spyOn(wrapper.vm, 'deleteComment')
    deleteCommentSpy('joshua', 'david')
    expect(wrapper.vm.deleting).toBeFalsy()
  })

  it('onDelete', () => {
    const onDeleteSpy = jest.spyOn(wrapper.vm, 'onDelete')
    onDeleteSpy('joshua', 'david')
    expect(wrapper.vm.deleting).toBeTruthy()
  })

  it('onConfirm', () => {
    const onConfirmSpy = jest.spyOn(wrapper.vm, 'onConfirm')
    onConfirmSpy(true)
    expect(wrapper.vm.deleting).toBeFalsy()
  })

  it('onConfirm', () => {
    const onConfirmSpy = jest.spyOn(wrapper.vm, 'onConfirm')
    onConfirmSpy(false)
    expect(wrapper.vm.deleting).toBeFalsy()
  })
})
