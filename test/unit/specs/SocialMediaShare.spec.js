import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import SocialMediaShare from '@/components/layout/SocialMediaShare.vue'
import Router from 'vue-router'
const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Router)

const store = new Vuex.Store({
  state: {
    sharedImage: '',
    shareStatus: '',
    errorMessage: '',
    loading: false,
    fillCanvasWithImage: 'image.jpg'
  },

  getters: {
    shareGetterData: (state) => { return { ...state } },
    imageGetterData: state => state.sharedImage
  }
})

describe('@components/layout/SocialMediaShare.vue', () => {
  const router = new Router({ path: 'routes' })

  beforeAll(() => {
    // magic on html canvas
    const canvas = document.createElement('canvas')
    canvas.id = 'canvas'
    canvas.width = 200
    canvas.height = 200
    document.body.appendChild(canvas)
  })
  const wrapper = shallowMount(SocialMediaShare, {
    localVue,
    store,
    router,
    propsData: {shareText: '',
      farmPerformance: {result: 'excellent'}
    }})
  const defineShareContentSpy = jest.spyOn(wrapper.vm, 'defineShareContent')
  it('renders the correct markup', () => {
    expect(wrapper.html()).toContain('<p class="share-message">Share with my friends</p>')
  })
  it('trigger click event on facebook', () => {
    wrapper.find('span').trigger('click')
    // expect(wrapper.vm.fillCanvasWithImage(jest.fn())).toBe(true)
    expect(wrapper.vm.loading).toBe(true)
  })

  it('defineShareContent should call store.commit', () => {
    wrapper.vm.$store.commit = jest.fn()
    const overideSpy = jest.spyOn(wrapper.vm, 'shareOverrideOGMeta')
    defineShareContentSpy()
    expect(wrapper.vm.$store.commit).toHaveBeenCalled()
    expect(overideSpy).toHaveBeenCalled()
  })

  it('watch imageGetter data should call defineShareContent', () => {
    wrapper.vm.$options.watch.imageGetterData.call(wrapper.vm, 'new value')
    expect(defineShareContentSpy).toHaveBeenCalled()
  })

  it('watch imageGetter data should call defineShareContent', () => {
    defineShareContentSpy.mockReset()
    wrapper.vm.$options.watch.imageGetterData.call(wrapper.vm, null)
    expect(defineShareContentSpy).not.toHaveBeenCalled()
  })
})
