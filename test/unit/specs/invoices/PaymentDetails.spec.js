import { shallowMount, createLocalVue } from '@vue/test-utils'
import PaymentDetails from '@/components/ui/paymentDetails/PaymentInvoiceDetails'
import Vue from 'vue'
import { eventBus } from '@/utils/helperFunctions'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)
const router = new VueRouter()

const EventBus = new Vue()

const GlobalPlugins = {
  install (v) {
    // Event bus
    v.prototype.$bus = EventBus
  }
}

localVue.use(GlobalPlugins)

describe('PaymentDetails', () => {
  let wrapper

  test('should trigger the closePaymentModal method', () => {
    wrapper = shallowMount(PaymentDetails)
    const spy = jest.spyOn(wrapper.vm, 'closePaymentModal')
    wrapper.setData({
      showModal: false,
      content: 'invoice'
    })
    wrapper.vm.closePaymentModal()
    expect(spy).toHaveBeenCalled()
  })

  it('eventBus bus on created should show form', () => {
    eventBus.$emit('showPaymentModal', 'invoice')
    expect(wrapper.vm.content).toEqual('invoice')
  })

  it('eventBus bus on created should not show payment modal', () => {
    eventBus.$emit('closeModal')
    expect(wrapper.vm.showPaymentModal).toEqual(false)
  })
})

describe('Payment Details State', () => {
  const getSingleInvoice = jest.fn()
  const resetInvoice = jest.fn()

  const store = new Vuex.Store({
    modules: {
      AllInvoicesModule: {
        state: {
          invoices: [],
          loading: false,
          singleInvoice: {},
          orderList: [],
          billing: {}
        },

        actions: { getSingleInvoice, resetInvoice }
      }
    }
  })

  const wrapper = shallowMount(PaymentDetails, {
    localVue,
    store,
    router
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should call "getInvoices " on mounting', () => {
    eventBus.$emit('showPaymentModal', ('invoices', 'payload'))
    wrapper.setData({
      showPaymentModal: true,
      content: 'invoices'
    })
    getSingleInvoice(1)
    expect(wrapper.vm.showPaymentModal).toBe(true)
  })

  it('should call "resetInvoices" on mounting', () => {
    expect(resetInvoice).toHaveBeenCalledTimes(2)
  })
})
